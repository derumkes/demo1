package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Role;
import com.umkes.demo1.utils.ConnectionFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static com.umkes.demo1.dao.RoleDaoImpl.GET_ALL_ROLES_SQL;
import static com.umkes.demo1.dao.RoleDaoImpl.GET_ROLE_BY_ID_SQL;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class RoleDaoImplTest {
    private RoleDaoImpl roleDao = new RoleDaoImpl();

    private Connection connection = mock(Connection.class);
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private ResultSet resultSet = mock(ResultSet.class);

    @BeforeMethod
    public void setUp() {
        ConnectionFactory.setConnection(connection);
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testGetRoleById() throws Exception {
        when(connection.prepareStatement(GET_ROLE_BY_ID_SQL)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("roleId")).thenReturn(1);
        when(resultSet.getString("roleName")).thenReturn("test");

        Role role = roleDao.getRoleById(1).get();

        assertEquals(1, role.getRoleId());
        assertEquals("test", role.getRoleName());
    }

    @Test
    public void testAllRoles() throws SQLException {
        when(connection.prepareStatement(GET_ALL_ROLES_SQL)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("roleId")).thenReturn(1).thenReturn(2);
        when(resultSet.getString("roleName")).thenReturn("test1").thenReturn("test2");

        ArrayList<Role> roles = (ArrayList<Role>) roleDao.allRoles();

        Role role = roles.get(0);
        assertEquals(1, role.getRoleId());
        assertEquals("test1", role.getRoleName());

        role = roles.get(1);
        assertEquals(2, role.getRoleId());
        assertEquals("test2", role.getRoleName());
    }
}