<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="WEB-INF/pages/includes/head_styles.jsp" %>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="home-panel panel panel-default">
            <div class="panel-heading">
                <h2 class="text-center">Welcome to AdvertiseBoard</h2>
            </div>
            <div class="panel-body">
                <div class="loginButtons">
                    <div class="width-100p">
                        <button class="btn btn-success width-90p"
                                onclick="location.href='<%= request.getContextPath() %>/logout'">
                            <span class="glyphicon glyphicon-log-in"></span>
                            Login
                        </button>
                    </div>
                    <div class="width-100p">
                        <button class="btn btn-info
                         width-90p" onclick="location.href='<%= request.getContextPath() %>/register'">
                            <span class="glyphicon glyphicon-plus-sign"></span>
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="WEB-INF/pages/includes/foot_scripts.jsp" %>