<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>

<%@include file="../includes/header.jsp" %>
<div class="centeredContent">
    <h2 class="center">Admin page. Hello ${userName}!</h2>
    <div class="row">
        <div class="col-md-4">
            <form action="<%= request.getContextPath() %>/admin/addUser" method="get">
                <button type="submit" class="btn btn-info margin-bottom-20p">
                    <em class="fa fa-plus"></em>
                    <span>Create new user</span>
                </button>
            </form>
        </div>
    </div>
    <div class="row width-90p margin-20">
        <div class="col-md-12">
            <c:choose>
                <c:when test="${not empty users}">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover width-100p">
                            <thead>
                            <tr>
                                <th>
                                    <span>UserId</span>
                                </th>
                                <th>
                                    <span>Email</span>
                                </th>
                                <th>
                                    <span>UserName</span>
                                </th>
                                <th>
                                    <span>Role</span>
                                </th>
                                <th>
                                    <span>Status</span>
                                </th>
                                <th>
                                    <div class="center-block fill-parent text-center fa fa-edit"></div>
                                </th>
                                <th>
                                    <div class="center-block fill-parent text-center fa fa-remove"></div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="user" items="${users}">
                                <tr>
                                    <td>
                                        <span><c:out value="${user.userId}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${user.email}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${user.userName}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${user.role.roleName}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${user.status}"/></span>
                                    </td>
                                    <td class="grid-cell-action">
                                        <form action="<%= request.getContextPath() %>/admin/editUser" method="get">
                                            <button type="submit"
                                                    name="edit"
                                                    class="btn"
                                                    value="<c:out value="${user.userId}"/>">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </form>
                                    </td>
                                    <td class="grid-cell-action">
                                        <form action="<%= request.getContextPath() %>/admin/delete"
                                              method="post">
                                            <button type="submit"
                                                    name="delete"
                                                    class="btn"
                                                    onclick="return confirm('Are you really sure you want to delete this user?');"
                                                    value="<c:out value="${user.userId}"/>">
                                                <span class="fa fa-remove"></span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <c:if test="${noOfPages != 1}">
                        <nav aria-label="Pagination centeredContent">
                            <ul class="pagination">
                                <c:if test="${currentPage != 1}">
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="<%= request.getContextPath() %>/admin?recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}">Previous</a>
                                    </li>
                                </c:if>

                                <c:forEach begin="1" end="${noOfPages}" var="i">
                                    <c:choose>
                                        <c:when test="${currentPage eq i}">
                                            <li class="page-item active"><a class="page-link">
                                                    ${i} <span class="sr-only">(current)</span></a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="<%= request.getContextPath() %>/admin?recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>

                                <c:if test="${currentPage lt noOfPages}">
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="<%= request.getContextPath() %>/admin?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}">Next</a>
                                    </li>
                                </c:if>
                            </ul>
                        </nav>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <h2 class="center">There is no registered users</h2>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<%@include file="../includes/foot_scripts.jsp" %>