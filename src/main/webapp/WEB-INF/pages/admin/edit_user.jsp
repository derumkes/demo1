<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/header.jsp" %>
<div class="col-md-4 col-md-offset-4">
    <div class="form-panel panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Change user information</h2>
        </div>
        <div class="panel-body">
            <form action="<%= request.getContextPath() %>/admin/editUser" method="post">
                <fieldset>
                    <div class="form-group">
                        <label for="types">Role</label>
                        <select id="types"
                                name="selectedRole"
                                class="form-control"
                                required>
                            <c:forEach var="item" items="${roles}">
                                <option value="${item.roleId}"
                                    ${item.roleId == user.role.roleId ? 'selected' : ''}>
                                        ${item.roleName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email"
                               name="email"
                               type="email"
                               class="form-control"
                               value="${user.email}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="userName">User name</label>
                        <input id="userName"
                               name="userName"
                               type="text"
                               value="${user.userName}"
                               class="form-control"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password"
                               name="password"
                               type="password"
                               value="${user.password}"
                               class="form-control"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select id="status"
                                name="status"
                                class="form-control"
                                required>
                            <option value="-1" ${"-1" == user.status ? 'selected' : ''}>Baned</option>
                            <option value="0" ${"0" == user.status? 'selected' : ''}>Disabled</option>
                            <option value="1" ${"1" == user.status ? 'selected' : ''}>Enabled</option>
                        </select>
                    </div>

                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger">
                            <c:out value="${errorMessage}"/>
                        </div>
                    </c:if>

                    <div>
                        <button type="submit"
                                name="edit"
                                value="<c:out value="${user.userId}"/>"
                                class="btn btn-lg btn-success btn-block">
                            Confirm
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<%@include file="../includes/foot_scripts.jsp" %>