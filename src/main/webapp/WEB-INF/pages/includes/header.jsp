<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header width-100p">
            <span class="navbar-brand">Advertise Board</span>
            <div class="pull-right">
                <a class="navbar-brand" href="<%= request.getContextPath() %>/logout">
                    <em class="glyphicon glyphicon-log-out"></em>
                    <span>Logout</span>
                </a>
            </div>
        </div>
    </div>
</nav>