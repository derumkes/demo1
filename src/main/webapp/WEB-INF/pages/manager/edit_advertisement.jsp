<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/header.jsp" %>
<div class="col-md-4 col-md-offset-4">
    <div class="form-panel panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Edit Advertisement</h2>
        </div>
        <div class="panel-body">
            <form action="<%= request.getContextPath() %>/mgr/edit" method="post">
                <fieldset>
                    <div class="form-group">
                        <label for="types">Type</label>
                        <select id="types"
                                name="selectedType"
                                class="form-control"
                                required>
                            <c:forEach var="item" items="${types}">
                                <option value="${item.typeId}"
                                    ${item.typeId == adv.type.typeId ? 'selected' : ''}>
                                        ${item.typeName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="advertisementText">Description</label>
                        <textarea id="advertisementText"
                                  name="advertisementText"
                                  maxlength="200"
                                  class="form-control" rows="5"
                                  required>
                            ${adv.text}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select id="status"
                                name="status"
                                class="form-control"
                                required>
                            <option value="-1" ${"-1" == adv.status ? 'selected' : ''}>Blocked</option>
                            <option value="0" ${"0" == adv.status? 'selected' : ''}>Not confirmed</option>
                            <option value="1" ${"1" == adv.status ? 'selected' : ''}>Confirmed</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="advertisementText">Comment</label>
                        <textarea id="comment"
                                  name="comment"
                                  maxlength="200"
                                  class="form-control" rows="5"
                                  required>
                            ${adv.comment}
                        </textarea>
                    </div>

                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger">
                            <c:out value="${errorMessage}"/>
                        </div>
                    </c:if>

                    <div>
                        <button type="submit"
                                name="edit"
                                value="<c:out value="${adv.advertisementId}"/>"
                                class="btn btn-lg btn-success btn-block">
                            Confirm
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>


<%@include file="../includes/foot_scripts.jsp" %>