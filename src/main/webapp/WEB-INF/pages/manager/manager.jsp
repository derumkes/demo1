<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/header.jsp" %>
<div class="centeredContent">
    <h2 class="center">Manager page. Hello ${user.userName}!</h2>

    <c:choose>
        <c:when test="${not empty advertisements}">
            <div class="row width-90p margin-20">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover width-100p">
                            <thead>
                            <tr>
                                <th>
                                    <span>Date</span>
                                </th>
                                <th>
                                    <span>Type</span>
                                </th>
                                <th>
                                    <span>Text</span>
                                </th>
                                <th>
                                    <span>User Name</span>
                                </th>
                                <th>
                                    <span>Email</span>
                                </th>
                                <th>
                                    <div class="center-block fill-parent text-center fa fa-edit"></div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${advertisements}">
                                <tr class="${item.status eq 1 ? 'success' : ''} ${item.status eq -1 ? 'danger' : ''}">
                                    <td class="width-100px">
                                        <span><c:out value="${item.date}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${item.type.typeName}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${item.text}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${item.user.userName}"/></span>
                                    </td>
                                    <td>
                                        <span><c:out value="${item.user.email}"/></span>
                                    </td>
                                    <td class="grid-cell-action">
                                        <form action="<%= request.getContextPath() %>/mgr/edit"
                                              method="get">
                                            <button type="submit"
                                                    name="edit"
                                                    class="btn"
                                                    value="<c:out value="${item.advertisementId}"/>">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <c:if test="${noOfPages != 1}">
                        <nav aria-label="Pagination centeredContent">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link"
                                       href="<%= request.getContextPath() %>/mgr?recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}">Previous</a>
                                </li>

                                <c:forEach begin="1" end="${noOfPages}" var="i">
                                    <c:choose>
                                        <c:when test="${currentPage eq i}">
                                            <li class="page-item active"><a class="page-link">
                                                    ${i} <span class="sr-only">(current)</span></a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="<%= request.getContextPath() %>/mgr?recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>

                                <c:if test="${currentPage lt noOfPages}">
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="<%= request.getContextPath() %>/mgr?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}">Next</a>
                                    </li>
                                </c:if>
                            </ul>
                        </nav>
                    </c:if>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <h2 class="center">There is no advertisements</h2>
        </c:otherwise>
    </c:choose>
</div>
<%@include file="../includes/foot_scripts.jsp" %>