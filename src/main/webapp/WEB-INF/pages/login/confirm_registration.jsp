<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/home_button.jsp" %>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="form-panel panel panel-default">
            <div class="panel-heading">
                <h2 class="text-center">Registration</h2>
            </div>
            <div class="panel-body">
                <c:choose>
                    <c:when test="${not empty errorMessage}">
                        <div class="alert alert-danger">
                            <c:out value="${errorMessage}"/>
                        </div>
                        You will be redirected to login page
                    </c:when>
                    <c:otherwise>
                        <form action="<%= request.getContextPath() %>/confirmRegistration" method="post">
                            <button type="submit"
                                    name="token"
                                    value="${token}"
                                    class="btn btn-lg btn-success btn-block">
                                Confirm registration
                            </button>
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@include file="../includes/foot_scripts.jsp" %>