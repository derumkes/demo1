<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/home_button.jsp" %>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h2 class="text-center">Login</h2>
            </div>
            <div class="panel-body">
                <form action="<%= request.getContextPath() %>/login" method="post">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control"
                                   placeholder="example@example.com"
                                   type="email"
                                   name="email"
                                   required
                                   autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control"
                                   placeholder="Password"
                                   type="password"
                                   name="password"
                                   required>
                        </div>

                        <c:if test="${not empty errorMessage}">
                            <div class="alert alert-danger">
                                <c:out value="${errorMessage}"/>
                            </div>
                        </c:if>

                        <div>
                            <button type="submit"
                                    value="submit"
                                    class="btn btn-lg btn-success btn-block">
                                Log In
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<%@include file="../includes/foot_scripts.jsp" %>