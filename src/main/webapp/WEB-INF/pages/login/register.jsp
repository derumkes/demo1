<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@include file="../includes/head_styles.jsp" %>
<%@include file="../includes/home_button.jsp" %>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="form-panel panel panel-default">
            <div class="panel-heading">
                <h2 class="text-center">Registration</h2>
            </div>
            <div class="panel-body">
                <c:choose>
                    <c:when test="${not empty successMessage}">
                        <div class="alert alert-success">
                            <c:out value="${successMessage}"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <form action="<%= request.getContextPath() %>/register" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control"
                                           placeholder="example@example.com"
                                           type="email"
                                           name="email"
                                           value="${email}"
                                           required
                                           autofocus>
                                </div>
                                <div class=" form-group">
                                    <input class="form-control"
                                           placeholder="User Name"
                                           name="userName"
                                           value="${userName}"
                                           required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control"
                                           placeholder="Password"
                                           title="Password must contain at least 6 characters, including UPPER/lowercase and numbers"
                                           name="password"
                                           type="password"
                                           value="${password}"
                                           required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
                                           onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
                                                     if(this.checkValidity()) form.password2.pattern = RegExp.escape(this.value);
                                                    ">
                                </div>
                                <div class="form-group">
                                    <input class="form-control"
                                           placeholder="Confirm Password"
                                           title="Please enter the same Password as above"
                                           type="password"
                                           name="password2"
                                           required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
                                           onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');">
                                </div>
                                <c:choose>
                                    <c:when test="${not empty errorMessage}">
                                        <form action="<%= request.getContextPath() %>/confirmRegistration"
                                              method="post">
                                            <div class="alert alert-danger">
                                                <c:out value="${errorMessage}"/>
                                            </div>
                                        </form>
                                    </c:when>
                                </c:choose>
                                <div>
                                    <button type="submit"
                                            value="submit"
                                            class="btn btn-lg btn-info btn-block">
                                        Register
                                    </button>
                                </div>
                            </fieldset>
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@include file="../includes/foot_scripts.jsp" %>