package com.umkes.demo1.entity;

import com.umkes.demo1.utils.Constants;

import java.io.Serializable;

public class User implements Serializable {
    private int userId;
    private String email;
    private String userName;
    private Role role;
    private String password;
    private int status;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getUserName() {
        return userName;
    }

    public Role getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public int getStatus() {
        return status;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", role=" + role +
                ", password='" + password + '\'' +
                ", status=" + status +
                '}';
    }

    public boolean isAdmin() {
        return getRole().getRoleId() == Constants.ROLE_ADMIN;
    }

    public boolean isUser() {
        return getRole().getRoleId() == Constants.ROLE_USER;
    }

    public boolean isManager() {
        return getRole().getRoleId() == Constants.ROLE_MANAGER;
    }

    public boolean isActive(){
        return getStatus() == 1;
    }
}
