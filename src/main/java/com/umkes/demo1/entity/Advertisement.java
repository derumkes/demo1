package com.umkes.demo1.entity;

import java.io.Serializable;
import java.time.LocalDate;

public class Advertisement implements Serializable {
    private int advertisementId;
    private String text;
    private User user;
    private int status;
    private LocalDate date;
    private Type type;
    private String comment;

    public Advertisement() {
        super();
    }

    public int getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(int advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "advertisementId=" + advertisementId +
                ", text='" + text + '\'' +
                ", user=" + user +
                ", status=" + status +
                ", date=" + date +
                ", type=" + type +
                ", comment='" + comment + '\'' +
                '}';
    }
}
