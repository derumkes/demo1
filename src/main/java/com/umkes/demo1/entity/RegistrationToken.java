package com.umkes.demo1.entity;

import java.time.LocalDate;
import java.util.UUID;

public class RegistrationToken {
    private UUID token;
    private User user;
    private LocalDate expireDate;

    public RegistrationToken() {
    }

    public RegistrationToken(User user) {
        this.user = user;
        this.token = UUID.randomUUID();
    }

    public UUID getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }


    public void setToken(UUID token) {
        this.token = token;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public String toString() {
        return "RegistrationToken{" +
                "token=" + token +
                ", user=" + user +
                ", expireDate=" + expireDate +
                '}';
    }
}
