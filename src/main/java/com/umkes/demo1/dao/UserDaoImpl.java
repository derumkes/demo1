package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Role;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.ConnectionFactory;
import com.umkes.demo1.utils.LogManager;
import com.umkes.demo1.utils.PasswordEncryption;

import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDaoImpl implements UserDao {
    private RoleDaoImpl roleDao;

    private static Logger LOGGER = LogManager.getLogger(UserDaoImpl.class.getName());

    private static final String ADD_USER_SQL =
            "INSERT INTO users(email, username, roleId, password, status)" +
                    "values (?, ?, ?, ?, ?)";

    private static final String GET_ALL_USERS =
            "select * from users LIMIT ? OFFSET ?";

    private static final String GET_ALL_USERS_COUNT =
            "select count(*) cnt from users";

    private static final String EDIT_USER_SQL =
            "UPDATE Users " +
                    "set email = ?, username = ?, roleid = ?, password = ?, status = ?" +
                    "where userId = ?";

    private static final String DELETE_USER_SQL =
            "DELETE FROM Users where userId = ?";

    private static final String GET_USER_BY_CRED_SQL =
            "select * " +
            "from users " +
            "where email = ?" +
            "and password = ?";

    private static final String GET_USER_BY_ID_SQL = "select * from users where userid = ?";

    private static final String GET_USER_BY_EMAIL_SQL = "select count(*) cnt from users where email = ?";


    @Inject
    public UserDaoImpl(RoleDaoImpl roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<User> allUsers(int currentPage, int recordsPerPage) {
        List<User> list = new ArrayList<>();
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_ALL_USERS)
        ) {
            int offset = currentPage * recordsPerPage - recordsPerPage;
            statement.setInt(1, recordsPerPage);
            statement.setInt(2, offset);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    list.add(parseUserFromResultSet(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting all users", e);
        }
        return list;
    }

    @Override
    public int allUsersCount() {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_ALL_USERS_COUNT)
        ) {
            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? resultSet.getInt("cnt") : 0;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting all users", e);
        }
    }

    @Override
    public User addUser(User user) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(ADD_USER_SQL, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getUserName());
            statement.setInt(3, user.getRole().getRoleId());
            statement.setString(4, PasswordEncryption.encrypt(user.getPassword()));
            statement.setInt(5, user.getStatus());

            LOGGER.info(statement.toString());

            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                generatedKeys.next();
                user.setUserId(generatedKeys.getInt(1));
                return user;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while creating user", e);
        }
    }

    @Override
    public User editUser(User user) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(EDIT_USER_SQL)
        ) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getUserName());
            statement.setInt(3, user.getRole().getRoleId());
            statement.setString(4, user.getPassword());
            statement.setInt(5, user.getStatus());
            statement.setInt(6, user.getUserId());

            LOGGER.info(statement.toString());
            statement.execute();

            return user;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while editing user", e);
        }
    }

    @Override
    public User deleteUser(User user) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(DELETE_USER_SQL)
        ) {
            statement.setInt(1, user.getUserId());

            LOGGER.info(statement.toString());
            statement.execute();

            return user;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while deleting user", e);
        }
    }

    @Override
    public Optional<User> getUserByEmailAndPassword(String email, String password) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_USER_BY_CRED_SQL)
        ) {
            statement.setString(1, email);
            statement.setString(2, password);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseUserFromResultSet(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting user", e);
        }
    }

    @Override
    public Optional<User> getUserById(int id) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_USER_BY_ID_SQL)
        ) {
            statement.setInt(1, id);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseUserFromResultSet(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting user", e);
        }
    }

    @Override
    public int checkEmail(String email) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_USER_BY_EMAIL_SQL)
        ) {
            statement.setString(1, email);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt("cnt");
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting user", e);
        }
    }

    private User parseUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getInt("userId"));
        user.setUserName(resultSet.getString("userName"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setStatus(resultSet.getInt("status"));
        Role role = roleDao.getRoleById(resultSet.getInt("roleId")).get();
        user.setRole(role);

        LOGGER.log(Level.FINE,"Created object from result set: {0}", user);

        return user;
    }
}
