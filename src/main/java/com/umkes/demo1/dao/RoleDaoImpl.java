package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Role;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.ConnectionFactory;
import com.umkes.demo1.utils.LogManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RoleDaoImpl implements RoleDao {
    private static Logger LOGGER = LogManager.getLogger(RoleDaoImpl.class.getName());

    static final String GET_ROLE_BY_ID_SQL = "SELECT * FROM roles where roleId = ?";
    static final String GET_ALL_ROLES_SQL = "SELECT * FROM roles";

    @Override
    public Optional<Role> getRoleById(int id) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_ROLE_BY_ID_SQL)
        ) {
            statement.setInt(1, id);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Role role = new Role();
                    role.setRoleId(resultSet.getInt("roleId"));
                    role.setRoleName(resultSet.getString("roleName"));
                    return Optional.of(role);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting Role by Id", e);
        }
    }

    @Override
    public List<Role> allRoles() {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_ALL_ROLES_SQL)
        ) {
            List<Role> list = new ArrayList<>();

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Role role = new Role();
                    role.setRoleId(resultSet.getInt("roleId"));
                    role.setRoleName(resultSet.getString("roleName"));
                    list.add(role);
                }
                return list;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting all roles", e);
        }
    }

}
