package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Role;
import com.umkes.demo1.entity.Type;

import java.util.List;
import java.util.Optional;

public interface RoleDao {
    Optional<Role> getRoleById(int id);
    List<Role> allRoles();
}
