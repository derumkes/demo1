package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.ConnectionFactory;
import com.umkes.demo1.utils.LogManager;

import javax.inject.Inject;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdvertisementDaoImpl implements AdvertisementDao {
    private UserDaoImpl userDao;
    private TypeDaoImpl typeDao;
    private static Logger LOGGER = LogManager.getLogger(AdvertisementDaoImpl.class.getName());

    private static final String ADD_ADVERTISEMENT_SQL =
            "INSERT INTO advertisements(text, userid, status, typeid) " +
                    "values (?, ?, ?, ?)";

    private static final String UPDATE_ADVERTISEMENT_SQL =
            "UPDATE advertisements " +
                    "set text = ?, userId = ?, status = ?, date = ?, typeId = ?, comment = ?" +
                    "where advertisementId = ?";

    private static final String SELECT_ALL_ADVERTISEMENT_SQL =
            "SELECT * FROM advertisements where status = 0 LIMIT ? OFFSET ?";

    private static final String SELECT_APPROVED_ADVERTISEMENT_SQL =
            "SELECT * FROM advertisements where status = 1  LIMIT ? OFFSET ?";

    private static final String SELECT_ALL_ADVERTISEMENT_BY_USER_ID_SQL =
            "SELECT * FROM advertisements where userId = ?  LIMIT ? OFFSET ?";

    private static final String SELECT_ALL_ADVERTISEMENT_COUNT_SQL =
            "SELECT count(*) cnt FROM advertisements where status = 0";

    private static final String SELECT_APPROVED_ADVERTISEMENT_COUNT_SQL =
            "SELECT count(*) cnt FROM advertisements where status = 1";

    private static final String SELECT_ALL_ADVERTISEMENT_BY_USER_ID_COUNT_SQL =
            "SELECT count(*) cnt FROM advertisements where userId = ?";

    private static final String DELETE_ADVERTISEMENT_SQL =
            "DELETE FROM advertisements where advertisementId = ?";

    private static final String SELECT_ADVERTISEMENT_BY_ID_SQL =
            "SELECT * FROM advertisements where advertisementId = ?";

    @Inject
    public AdvertisementDaoImpl(UserDaoImpl userDao, TypeDaoImpl typeDao) {
        this.userDao = userDao;
        this.typeDao = typeDao;
    }

    @Override
    public List<Advertisement> allAdvertisements(int currentPage, int recordsPerPage) {
        return getAdvertisements(currentPage, recordsPerPage, SELECT_ALL_ADVERTISEMENT_SQL);
    }

    private List<Advertisement> getAdvertisements(int currentPage, int recordsPerPage, String selectAllAdvertisementSql) {
        List<Advertisement> list = new ArrayList<>();
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(selectAllAdvertisementSql)
        ) {
            int offset = currentPage * recordsPerPage - recordsPerPage;
            statement.setInt(1, recordsPerPage);
            statement.setInt(2, offset);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    list.add(parseAdvertisementFromResultSet(resultSet));
                }
                return list;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting all advertisements", e);
        }
    }

    @Override
    public List<Advertisement> approvedAdvertisements(int currentPage, int recordsPerPage) {
        return getAdvertisements(currentPage, recordsPerPage, SELECT_APPROVED_ADVERTISEMENT_SQL);
    }

    @Override
    public List<Advertisement> userAdvertisements(int userId, int currentPage, int recordsPerPage) {
        List<Advertisement> list = new ArrayList<>();
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(SELECT_ALL_ADVERTISEMENT_BY_USER_ID_SQL)
        ) {
            statement.setInt(1, userId);

            int offset = currentPage * recordsPerPage - recordsPerPage;
            statement.setInt(2, recordsPerPage);
            statement.setInt(3, offset);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    list.add(parseAdvertisementFromResultSet(resultSet));
                }
            }
            return list;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting user advertisements", e);
        }
    }

    @Override
    public int allAdvertisementsCount() {
        return getResultSetCount(SELECT_ALL_ADVERTISEMENT_COUNT_SQL);
    }

    @Override
    public int approvedAdvertisementsCount() {
        return getResultSetCount(SELECT_APPROVED_ADVERTISEMENT_COUNT_SQL);
    }

    private int getResultSetCount(String selectApprovedAdvertisementCountSql) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(selectApprovedAdvertisementCountSql)
        ) {
            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? resultSet.getInt("cnt") : 0;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting recordCount", e);
        }
    }

    @Override
    public int userAdvertisementsCount(int userId) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(SELECT_ALL_ADVERTISEMENT_BY_USER_ID_COUNT_SQL)
        ) {
            statement.setInt(1, userId);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? resultSet.getInt("cnt") : 0;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting user advertisements", e);
        }
    }

    @Override
    public Advertisement addAdvertisement(Advertisement advertisement) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement =
                        Objects.requireNonNull(connection)
                                .prepareStatement(
                                        ADD_ADVERTISEMENT_SQL,
                                        Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, advertisement.getText().trim().length() > 200 ? advertisement.getText().trim().substring(0, 200) : advertisement.getText().trim());
            statement.setInt(2, advertisement.getUser().getUserId());
            statement.setInt(3, advertisement.getStatus());
            statement.setInt(4, advertisement.getType().getTypeId());

            statement.executeUpdate();
            LOGGER.info(statement.toString());
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                generatedKeys.next();
                advertisement.setAdvertisementId(generatedKeys.getInt(1));
                return advertisement;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while adding advertisement", e);
        }
    }

    @Override
    public Advertisement editAdvertisement(Advertisement advertisement) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(UPDATE_ADVERTISEMENT_SQL)
        ) {
            statement.setString(1, advertisement.getText().trim().length() > 200 ? advertisement.getText().trim().substring(0, 200) : advertisement.getText().trim());
            statement.setInt(2, advertisement.getUser().getUserId());
            statement.setInt(3, advertisement.getStatus());
            statement.setObject(4, advertisement.getDate());
            statement.setInt(5, advertisement.getType().getTypeId());
            statement.setString(6, advertisement.getComment());
            statement.setInt(7, advertisement.getAdvertisementId());

            LOGGER.info(statement.toString());
            statement.executeUpdate();

            return advertisement;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while editing advertisement", e);
        }
    }

    @Override
    public Advertisement deleteAdvertisement(Advertisement advertisement) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(DELETE_ADVERTISEMENT_SQL)
        ) {
            statement.setInt(1, advertisement.getAdvertisementId());

            LOGGER.info(statement.toString());
            statement.executeUpdate();

            return advertisement;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while deleting advertisement", e);
        }
    }

    @Override
    public Optional<Advertisement> getAdvertisementById(int id) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(SELECT_ADVERTISEMENT_BY_ID_SQL)
        ) {
            statement.setInt(1, id);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseAdvertisementFromResultSet(resultSet));
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting advertisement by id", e);
        }
    }

    private Advertisement parseAdvertisementFromResultSet(ResultSet resultSet) throws SQLException {
        Advertisement advertisement = new Advertisement();
        advertisement.setAdvertisementId(resultSet.getInt("advertisementId"));
        advertisement.setDate(resultSet.getObject("date", LocalDate.class));
        advertisement.setText(resultSet.getString("text").trim());
        advertisement.setStatus(resultSet.getInt("status"));
        advertisement.setComment(resultSet.getString("comment"));
        advertisement.setUser(userDao.getUserById(resultSet.getInt("userId")).get()); //empty user unreachable case
        advertisement.setType(typeDao.getTypeById(resultSet.getInt("typeId")).get()); //empty type unreachable case

        LOGGER.log(Level.FINE, "Created object from result set: {0}", advertisement);

        return advertisement;
    }
}
