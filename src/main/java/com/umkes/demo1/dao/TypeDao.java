package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Type;

import java.util.List;
import java.util.Optional;

public interface TypeDao {
    List<Type> allTypes();

    Optional<Type> getTypeById(int id);

    Type addType(Type type);
}
