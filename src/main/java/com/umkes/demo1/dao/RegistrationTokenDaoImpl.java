package com.umkes.demo1.dao;

import com.umkes.demo1.entity.RegistrationToken;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.ConnectionFactory;
import com.umkes.demo1.utils.LogManager;

import javax.inject.Inject;
import java.sql.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrationTokenDaoImpl implements RegistrationTokenDao {
    private UserDaoImpl userDao;

    private static Logger LOGGER = LogManager.getLogger(RegistrationTokenDaoImpl.class.getName());

    private static final String SELECT_TOKEN_BY_ID_SQL =
            "SELECT * FROM registration_tokens where token = ?";

    private static final String ADD_TOKEN_SQL =
            "INSERT INTO registration_tokens(token, userId) " +
                    "values (?, ?)";

    private static final String DELETE_TOKEN_SQL =
            "DELETE FROM registration_tokens where token = ?";

    @Inject
    public RegistrationTokenDaoImpl(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    @Override
    public Optional<RegistrationToken> getTokenById(UUID token) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).
                        prepareStatement(SELECT_TOKEN_BY_ID_SQL)
        ) {
            statement.setObject(1, token);

            LOGGER.info(statement.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseRegistrationTokenFromResultSet(resultSet));
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting registration token by id", e);
        }
    }

    @Override
    public RegistrationToken addToken(RegistrationToken token) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement =
                        Objects.requireNonNull(connection)
                                .prepareStatement(
                                        ADD_TOKEN_SQL,
                                        Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setObject(1, token.getToken());
            statement.setInt(2, token.getUser().getUserId());

            LOGGER.info(statement.toString());
            statement.executeUpdate();
            return token;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while adding registration token", e);
        }
    }

    @Override
    public RegistrationToken deleteToken(RegistrationToken token) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).
                        prepareStatement(DELETE_TOKEN_SQL)
        ) {
            statement.setObject(1, token.getToken());

            LOGGER.info(statement.toString());
            statement.executeUpdate();

            return token;
        } catch (SQLException e) {
            throw new AdvertisementException("Error while deleting advertisement", e);
        }
    }

    private RegistrationToken parseRegistrationTokenFromResultSet(ResultSet resultSet) throws SQLException {
        RegistrationToken registrationToken = new RegistrationToken();
        registrationToken.setToken(resultSet.getObject("token", UUID.class));
        registrationToken.setUser(userDao.getUserById(resultSet.getInt("userId")).get()); //empty user unreachable case
        registrationToken.setExpireDate(resultSet.getObject("expire_date", LocalDate.class));

        LOGGER.log(Level.FINE, "Created object from result set: {0}", registrationToken);

        return registrationToken;
    }
}
