package com.umkes.demo1.dao;

import com.umkes.demo1.entity.RegistrationToken;

import java.util.Optional;
import java.util.UUID;

public interface RegistrationTokenDao {
    Optional<RegistrationToken> getTokenById(UUID token);

    RegistrationToken addToken(RegistrationToken token);

    RegistrationToken deleteToken(RegistrationToken token);
}
