package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Advertisement;

import java.util.List;
import java.util.Optional;

public interface AdvertisementDao {
    List<Advertisement> allAdvertisements(int currentPage, int recordsPerPage);
    List<Advertisement> approvedAdvertisements(int currentPage, int recordsPerPage);
    List<Advertisement> userAdvertisements(int userId, int currentPage, int recordsPerPage);
    int allAdvertisementsCount();
    int approvedAdvertisementsCount();
    int userAdvertisementsCount(int userId);

    Advertisement addAdvertisement(Advertisement advertisement);
    Advertisement editAdvertisement(Advertisement advertisement);
    Advertisement deleteAdvertisement(Advertisement advertisement);
    Optional<Advertisement> getAdvertisementById(int id);
}
