package com.umkes.demo1.dao;

import com.umkes.demo1.entity.Type;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.ConnectionFactory;
import com.umkes.demo1.utils.LogManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TypeDaoImpl implements TypeDao {

    private static Logger LOGGER = LogManager.getLogger(TypeDaoImpl.class.getName());

    private static final String ADD_TYPE_SQL =
            "INSERT INTO types(typeName) values (?)";

    private static final String GET_ALL_TYPES_SQL =
            "SELECT * FROM types";

    private static final String GET_TYPE_BY_ID_SQL =
            "SELECT * FROM types where typeId = ?";

    @Override
    public List<Type> allTypes() {
        List<Type> list = new ArrayList<>();
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_ALL_TYPES_SQL)
        ) {

            LOGGER.info(statement.toString());
            try(ResultSet resultSet = statement.executeQuery()){
                while (resultSet.next()) {
                    Type type = new Type();
                    type.setTypeId(resultSet.getInt("typeId"));
                    type.setTypeName(resultSet.getString("typeName"));
                    list.add(type);
                }
                return list;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting all types", e);
        }
    }

    @Override
    public Optional<Type> getTypeById(int id) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(GET_TYPE_BY_ID_SQL)
        ) {
            statement.setInt(1, id);

            LOGGER.info(statement.toString());
            try(ResultSet resultSet = statement.executeQuery()){
                Type type = new Type();
                if (resultSet.next()) {

                    type.setTypeId(resultSet.getInt("typeId"));
                    type.setTypeName(resultSet.getString("typeName").trim());
                }
                return Optional.of(type);
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while getting Type by Id", e);
        }
    }

    @Override
    public Type addType(Type type) {
        try (
                Connection connection = ConnectionFactory.getConnection();
                PreparedStatement statement = Objects.requireNonNull(connection).prepareStatement(ADD_TYPE_SQL, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, type.getTypeName());
            LOGGER.info(statement.toString());

            statement.executeUpdate();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                generatedKeys.next();
                type.setTypeId(generatedKeys.getInt(1));

                return type;
            }
        } catch (SQLException e) {
            throw new AdvertisementException("Error while adding Type", e);
        }
    }
}
