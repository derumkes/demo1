package com.umkes.demo1.dao;

import com.umkes.demo1.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    List<User> allUsers(int currentPage, int recordsPerPage);
    int allUsersCount();
    User addUser(User user);
    User editUser(User user);
    User deleteUser(User user);
    Optional<User> getUserById(int id);

    Optional<User> getUserByEmailAndPassword(String email, String password);
    int checkEmail(String email);
}
