package com.umkes.demo1.services;

import com.umkes.demo1.dao.TypeDaoImpl;
import com.umkes.demo1.entity.Type;
import com.umkes.demo1.exceptions.AdvertisementException;

import javax.inject.Inject;
import java.util.List;

public class TypeServiceImpl implements TypeService {
    private TypeDaoImpl typeDao;

    @Inject
    public TypeServiceImpl(TypeDaoImpl typeDao) {
        this.typeDao = typeDao;
    }

    @Override
    public List<Type> allTypes() {
        return typeDao.allTypes();
    }

    @Override
    public Type getTypeById(int id) {
        return typeDao.getTypeById(id)
                .orElseThrow(() -> new AdvertisementException("No data found"));
    }

    @Override
    public void addType(Type type) {
        typeDao.addType(type);
    }
}
