package com.umkes.demo1.services;

import com.umkes.demo1.entity.User;

import java.util.List;

public interface UserService {
    List<User> allUsers(int currentPage, int recordsPerPage);
    int allUsersCount();
    void addUser(User user);
    void editUser(User user);
    void deleteUser(User user);
    User getUserById(int id);

    User getUserByCredentials(String email, String password);
    void register(User user);
    boolean isEmailValid(String email);
}
