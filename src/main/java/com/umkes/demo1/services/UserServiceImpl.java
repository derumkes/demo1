package com.umkes.demo1.services;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.RegistrationToken;
import com.umkes.demo1.entity.Role;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.utils.Constants;
import com.umkes.demo1.utils.EmailSender;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDaoImpl dao;
    private RegistrationTokenDaoImpl tokenDao;

    public UserServiceImpl(UserDaoImpl dao, RegistrationTokenDaoImpl tokenDao) {
        this.dao = dao;
        this.tokenDao = tokenDao;
    }

    @Override
    public List<User> allUsers(int currentPage, int recordsPerPage) {
        return dao.allUsers(currentPage, recordsPerPage);
    }

    @Override
    public int allUsersCount() {
        return dao.allUsersCount();
    }

    @Override
    public void addUser(User user) {
        dao.addUser(user);
    }

    @Override
    public void editUser(User user) {
        dao.editUser(user);
    }

    @Override
    public void deleteUser(User user) {
        dao.deleteUser(user);
    }

    @Override
    public User getUserById(int id) {
        return dao.getUserById(id)
                .orElseThrow(() -> new AdvertisementException("No data found"));
    }

    @Override
    public User getUserByCredentials(String email, String password) {
        return dao.getUserByEmailAndPassword(email, password).orElseGet(User::new);
    }

    @Override
    public void register(User user) {
        Role role = new Role();
        role.setRoleId(Constants.ROLE_USER);

        user.setRole(role);
        user.setStatus(0);
        user = dao.addUser(user);
        RegistrationToken token = new RegistrationToken(user);
        tokenDao.addToken(token);

        sendRegistrationEmail(token);
    }

    @Override
    public boolean isEmailValid(String email) {
        return dao.checkEmail(email) == 0;
    }

    public void sendRegistrationEmail(RegistrationToken token) {
        EmailSender mail = new EmailSender();
        mail.setMailSubject("Account details for " + token.getUser().getUserName() + " at Advertisement Board");
        mail.setMailText(
                "Hello " + token.getUser().getUserName() + "! \n" +
                        "Your account is created and must be activate before you can use id.\n" +
                        "To activate the account click on the following link or copy-paste it in your browser: \n" +
                        Constants.HOST_URL + "/confirmRegistration?token=" + token.getToken().toString() + "\n" +
                        "\n" +
                        "After activation you may login to " + Constants.HOST_URL + "/login using your email and password"
        );
        mail.setReceiverEmail(token.getUser().getEmail());
        mail.send();
    }
}
