package com.umkes.demo1.services;

import com.umkes.demo1.entity.RegistrationToken;
import com.umkes.demo1.entity.Role;

import java.util.List;
import java.util.UUID;

public interface RegistrationTokenService {
    RegistrationToken getTokenById(UUID token);
    RegistrationToken deleteToken(RegistrationToken token);
}
