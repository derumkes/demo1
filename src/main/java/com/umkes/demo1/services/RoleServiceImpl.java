package com.umkes.demo1.services;

import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.entity.Role;
import com.umkes.demo1.exceptions.AdvertisementException;

import javax.inject.Inject;
import java.util.List;

public class RoleServiceImpl implements RoleService {
    private RoleDaoImpl roleDao;

    @Inject
    public RoleServiceImpl(RoleDaoImpl roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Role getRoleById(int id) {
        return roleDao.getRoleById(id)
                .orElseThrow(() -> new AdvertisementException("No data found"));
    }

    @Override
    public List<Role> allRoles() {
        return roleDao.allRoles();
    }
}
