package com.umkes.demo1.services;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.entity.RegistrationToken;
import com.umkes.demo1.exceptions.AdvertisementException;

import javax.inject.Inject;
import java.util.UUID;

public class RegistrationTokenServiceImpl implements RegistrationTokenService{

    private RegistrationTokenDaoImpl registrationTokenDao;

    @Inject
    public RegistrationTokenServiceImpl(RegistrationTokenDaoImpl registrationTokenDao) {
        this.registrationTokenDao = registrationTokenDao;
    }

    @Override
    public RegistrationToken getTokenById(UUID token) {
        return registrationTokenDao.getTokenById(token)
                .orElseThrow(() -> new AdvertisementException("No data found"));
    }

    @Override
    public RegistrationToken deleteToken(RegistrationToken token) {
        return registrationTokenDao.deleteToken(token);
    }
}
