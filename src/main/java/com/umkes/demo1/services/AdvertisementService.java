package com.umkes.demo1.services;

import com.umkes.demo1.entity.Advertisement;

import java.util.List;

public interface AdvertisementService {
    List<Advertisement> allAdvertisements(int currentPage, int recordsPerPage);

    List<Advertisement> approvedAdvertisements(int currentPage, int recordsPerPage);

    List<Advertisement> userAdvertisements(int userId, int currentPage, int recordsPerPage);

    int allAdvertisementsCount();

    int approvedAdvertisementsCount();

    int userAdvertisementsCount(int userId);

    Advertisement addAdvertisement(Advertisement advertisement);

    Advertisement editAdvertisement(Advertisement advertisement);

    Advertisement deleteAdvertisement(Advertisement advertisement);

    Advertisement getAdvertisementById(int id);
}
