package com.umkes.demo1.services;

import com.umkes.demo1.entity.Role;

import java.util.List;

public interface RoleService {
    Role getRoleById(int id);

    List<Role> allRoles();
}
