package com.umkes.demo1.services;

import com.umkes.demo1.entity.Type;

import java.util.List;

public interface TypeService {
    List<Type> allTypes();
    Type getTypeById(int id);
    void addType(Type type);
}
