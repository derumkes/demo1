package com.umkes.demo1.services;

import com.umkes.demo1.dao.AdvertisementDaoImpl;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.exceptions.AdvertisementException;

import javax.inject.Inject;
import java.util.List;

public class AdvertisementServiceImpl implements AdvertisementService {
    private AdvertisementDaoImpl advertisementDao;

    @Inject
    public AdvertisementServiceImpl(AdvertisementDaoImpl advertisementDao) {
        this.advertisementDao = advertisementDao;
    }

    @Override
    public List<Advertisement> approvedAdvertisements(int currentPage, int recordsPerPage) {
        return advertisementDao.approvedAdvertisements(currentPage, recordsPerPage);
    }

    @Override
    public List<Advertisement> allAdvertisements(int currentPage, int recordsPerPage) {
        return advertisementDao.allAdvertisements(currentPage, recordsPerPage);
    }

    @Override
    public List<Advertisement> userAdvertisements(int userId, int currentPage, int recordsPerPage) {
        return advertisementDao.userAdvertisements(userId, currentPage, recordsPerPage);
    }

    @Override
    public int allAdvertisementsCount() {
        return advertisementDao.allAdvertisementsCount();
    }

    @Override
    public int approvedAdvertisementsCount() {
        return advertisementDao.approvedAdvertisementsCount();
    }

    @Override
    public int userAdvertisementsCount(int userId) {
        return advertisementDao.userAdvertisementsCount(userId);
    }

    @Override
    public Advertisement addAdvertisement(Advertisement advertisement) {
        return advertisementDao.addAdvertisement(advertisement);
    }

    @Override
    public Advertisement editAdvertisement(Advertisement advertisement) {
        return advertisementDao.editAdvertisement(advertisement);
    }

    @Override
    public Advertisement deleteAdvertisement(Advertisement advertisement) {
        return advertisementDao.deleteAdvertisement(advertisement);
    }

    @Override
    public Advertisement getAdvertisementById(int id) {
        return advertisementDao.getAdvertisementById(id)
                .orElseThrow(() -> new AdvertisementException("No data found"));
    }
}
