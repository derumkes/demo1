package com.umkes.demo1.exceptions;

public class AdvertisementException extends RuntimeException {
    public AdvertisementException(String message, Throwable cause) {
        super(message, cause);
    }

    public AdvertisementException(Throwable cause) {
        super(cause);
    }

    public AdvertisementException(String message) {
        super(message);
    }
}
