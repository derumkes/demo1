package com.umkes.demo1.controller.advertisements;


import com.umkes.demo1.dao.*;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.Type;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.services.TypeServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/advertisements/add")
public class AddAdvertisementController extends HttpServlet {
    private AdvertisementServiceImpl advertisementService;
    private TypeServiceImpl typeService;
    private UserServiceImpl userService;

    @Inject
    public void init() {
        this.advertisementService = new AdvertisementServiceImpl(new AdvertisementDaoImpl(new UserDaoImpl(new RoleDaoImpl()), new TypeDaoImpl()));
        this.typeService = new TypeServiceImpl(new TypeDaoImpl());
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("types", typeService.allTypes());
        req.getRequestDispatcher(Constants.ADD_ADVERTISEMENT_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Type type = typeService.getTypeById(Integer.parseInt(req.getParameter("selectedType")));
        String text = req.getParameter("advertisementText");
        User requestUser = (User) req.getSession().getAttribute("user");
        User user = userService.getUserByCredentials(requestUser.getEmail(), requestUser.getPassword());

        if (type != null && !text.isEmpty() && user.isActive() && user.isUser()){
            Advertisement bean = new Advertisement();
            bean.setText(text);
            bean.setType(type);
            bean.setUser(user);
            bean.setStatus(0);
            advertisementService.addAdvertisement(bean);
            resp.sendRedirect(req.getContextPath() + "/advertisements");
        } else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
