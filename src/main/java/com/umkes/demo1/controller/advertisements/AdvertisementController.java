package com.umkes.demo1.controller.advertisements;

import com.umkes.demo1.dao.*;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/advertisements")
public class AdvertisementController extends HttpServlet {

    private AdvertisementServiceImpl advertisementService;
    private UserServiceImpl userService;

    @Inject
    public void init() {
        this.advertisementService = new AdvertisementServiceImpl(new AdvertisementDaoImpl(new UserDaoImpl(new RoleDaoImpl()), new TypeDaoImpl()));
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User requestUser = (User) req.getSession().getAttribute("user");
        User user = userService.getUserByCredentials(requestUser.getEmail(), requestUser.getPassword());

        ArrayList<Advertisement> list;
        int rowCount;
        boolean userAdvertisements = true;
        if (req.getParameterMap().containsKey("all")) {
            if (req.getParameter("all").equals("1")) {
                userAdvertisements = false;
            }
        }

        int currentPage = req.getParameterMap().containsKey("currentPage") ? Integer.valueOf(req.getParameter("currentPage")) : 1;
        int recordsPerPage = req.getParameterMap().containsKey("recordsPerPage") ? Integer.valueOf(req.getParameter("recordsPerPage")) : 5;

        if (userAdvertisements) {
            list = (ArrayList<Advertisement>) advertisementService.userAdvertisements(user.getUserId(), currentPage, recordsPerPage);
            rowCount = advertisementService.userAdvertisementsCount(user.getUserId());
            processList(req, list, currentPage, recordsPerPage, rowCount);
            req.getRequestDispatcher(Constants.ADVERTISEMENT_JSP).forward(req, resp);
        } else {
            list = (ArrayList<Advertisement>) advertisementService.approvedAdvertisements(currentPage, recordsPerPage);
            rowCount = advertisementService.approvedAdvertisementsCount();
            processList(req, list, currentPage, recordsPerPage, rowCount);
            req.getRequestDispatcher(Constants.ALL_ADVERTISEMENT_JSP).forward(req, resp);
        }
    }

    private void processList(HttpServletRequest req, ArrayList<Advertisement> list, int currentPage, int recordsPerPage, int rowCount) {
        int noOfPages = rowCount / recordsPerPage;

        if (rowCount % recordsPerPage > 0) {
            noOfPages++;
        }
        req.setAttribute("advertisements", list);
        req.setAttribute("noOfPages", noOfPages);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("recordsPerPage", recordsPerPage);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
