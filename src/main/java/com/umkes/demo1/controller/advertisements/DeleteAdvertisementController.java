package com.umkes.demo1.controller.advertisements;

import com.umkes.demo1.dao.*;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.services.TypeServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/advertisements/delete")
public class DeleteAdvertisementController extends HttpServlet {
    private AdvertisementServiceImpl advertisementService;
    private TypeServiceImpl typeService;
    private UserServiceImpl userService;

    @Inject
    public void init() {
        this.advertisementService = new AdvertisementServiceImpl(new AdvertisementDaoImpl(new UserDaoImpl(new RoleDaoImpl()), new TypeDaoImpl()));
        this.typeService = new TypeServiceImpl(new TypeDaoImpl());
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User requestUser = (User) req.getSession().getAttribute("user");
        if (requestUser == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }

        Advertisement adv = advertisementService.getAdvertisementById(Integer.parseInt(req.getParameter("delete")));
        if (adv.getUser().getUserId() != requestUser.getUserId()) {
            return;
        }

        if (adv != null) {
            advertisementService.deleteAdvertisement(adv);
            resp.sendRedirect(req.getContextPath() + "/advertisements");
        } else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
