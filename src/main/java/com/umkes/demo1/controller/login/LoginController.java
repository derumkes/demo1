package com.umkes.demo1.controller.login;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;
import com.umkes.demo1.utils.PasswordEncryption;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private UserServiceImpl userService;

    @Override
    @Inject
    public void init() {
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        login(req, resp, false, req.getParameter("email"), PasswordEncryption.encrypt(req.getParameter("password")));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object payload = req.getSession().getAttribute("user");
        try {
            User user = (User) payload;
            login(req, resp, true, user.getEmail(), user.getPassword());
        } catch (Exception e) {
            req.getRequestDispatcher(Constants.LOGIN_JSP).forward(req, resp);
        }
    }

    private void login(HttpServletRequest req, HttpServletResponse resp, boolean silent, String email, String password) throws ServletException, IOException {
        User user = userService.getUserByCredentials(email, password);
        if (!user.isActive()) {
            if (!silent) {
                req.setAttribute("errorMessage",
                        "You do not have access to resource");
            }
            req.getRequestDispatcher(Constants.LOGIN_JSP).forward(req, resp);
            return;
        }

        if (user.isUser()) {
            req.getSession().setAttribute("user", user);
            resp.sendRedirect(req.getContextPath() + "/advertisements");
        } else if (user.isManager()) {
            req.getSession().setAttribute("user", user);
            resp.sendRedirect(req.getContextPath() + "/mgr");
        } else if (user.isAdmin()) {
            req.getSession().setAttribute("user", user);
            resp.sendRedirect(req.getContextPath() + "/admin");
        } else {
            if (!silent) {
                req.setAttribute("errorString", "You do not have access to resource");
//            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }

            req.getRequestDispatcher(Constants.LOGIN_JSP).forward(req, resp);
        }
    }

}
