package com.umkes.demo1.controller.login;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.RegistrationToken;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.RegistrationTokenServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@WebServlet("/confirmRegistration")
public class ConfirmRegistrationController extends HttpServlet {
    private UserServiceImpl userService;
    private RegistrationTokenServiceImpl registrationTokenService;

    @Inject
    public void init() {
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
        this.registrationTokenService = new RegistrationTokenServiceImpl(new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = req.getParameter("token");
        verifyToken(token, req, resp);
        req.setAttribute("token", token);
        req.getRequestDispatcher(Constants.CONFIRM_REGISTRATION_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String token = req.getParameter("token");
        Optional<RegistrationToken> registrationToken = verifyToken(token, req, resp);
        if (registrationToken.isPresent()){
            User user = registrationToken.get().getUser();
            user.setStatus(1);
            userService.editUser(user);
            registrationTokenService.deleteToken(registrationToken.get());
            req.getSession().setAttribute("user", user);
            resp.sendRedirect(req.getContextPath() + "/login");
        } else {
            req.getRequestDispatcher(Constants.CONFIRM_REGISTRATION_JSP).forward(req, resp);
        }
    }

    private Optional<RegistrationToken> verifyToken(String token, HttpServletRequest req, HttpServletResponse resp) {
        RegistrationToken registrationToken;
        try {
            registrationToken = registrationTokenService.getTokenById(UUID.fromString(token));
        } catch (Exception e) {
            req.setAttribute("errorMessage",
                    "Invalid token.");
            resp.setHeader("Refresh", "3; URL = login");
            return Optional.empty();
        }
        if (registrationToken != null) {
            if (registrationToken.getExpireDate().isBefore(LocalDate.now())) {
                req.setAttribute("errorMessage",
                        "Token has been expired. Resending email.");
                resp.setHeader("Refresh", "3; URL = login");
                registrationTokenService.deleteToken(registrationToken);
                userService.sendRegistrationEmail(new RegistrationToken(registrationToken.getUser()));
                return Optional.empty();
            }
        }
        return Optional.of(registrationToken);
    }
}
