package com.umkes.demo1.controller.admin;


import com.umkes.demo1.dao.*;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.exceptions.AdvertisementException;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.services.TypeServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/delete")
public class DeleteUserController extends HttpServlet {
    private UserServiceImpl userService;

    @Inject
    public void init() {
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User requestUser = (User) req.getSession().getAttribute("user");
        if (requestUser == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }

        User user = userService.getUserById(Integer.parseInt(req.getParameter("delete")));

        if (user != null && requestUser.getUserId() != user.getUserId()) {
            userService.deleteUser(user);
            resp.sendRedirect(req.getContextPath() + "/admin");
        } else {
            throw new AdvertisementException("You can't delete yourself");
        }
    }
}
