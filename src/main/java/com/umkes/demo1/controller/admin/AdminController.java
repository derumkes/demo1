package com.umkes.demo1.controller.admin;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/admin")
public class AdminController extends HttpServlet {
    private UserServiceImpl userService;

    @Inject
    public void init() {
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int currentPage = req.getParameterMap().containsKey("currentPage") ? Integer.valueOf(req.getParameter("currentPage")) : 1;
        int recordsPerPage = req.getParameterMap().containsKey("recordsPerPage") ? Integer.valueOf(req.getParameter("recordsPerPage")) : 5;
        ArrayList<User> list = (ArrayList<User>) userService.allUsers(currentPage, recordsPerPage);
        int rowCount = userService.allUsersCount();
        int noOfPages = rowCount / recordsPerPage;

        if (rowCount % recordsPerPage > 0) {
            noOfPages++;
        }
        req.setAttribute("noOfPages", noOfPages);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("recordsPerPage", recordsPerPage);
        req.setAttribute("users", list);

        req.getRequestDispatcher(Constants.ADMIN_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
