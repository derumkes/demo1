package com.umkes.demo1.controller.admin;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.Role;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.RoleServiceImpl;
import com.umkes.demo1.services.UserServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/admin/addUser")
public class AddUserController extends HttpServlet {
    private UserServiceImpl userService;
    private RoleServiceImpl roleService;

    @Inject
    public void init() {
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
        this.roleService = new RoleServiceImpl(new RoleDaoImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Role> list = (ArrayList<Role>) roleService.allRoles();
        req.setAttribute("roles", list);

        req.getRequestDispatcher(Constants.ADMIN_ADD_USER_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Role role = roleService.getRoleById(Integer.parseInt(req.getParameter("selectedRole")));
        String email = req.getParameter("email");

        if (!userService.isEmailValid(email)) {
            req.setAttribute("errorMessage",
                    "Email is used for another user");
            req.getRequestDispatcher(Constants.ADMIN_ADD_USER_JSP).forward(req, resp);
            return;
        }

        String username = req.getParameter("userName");
        String password = req.getParameter("password");
        int status = 1; //auto confirm

        if (role != null && !username.isEmpty() && !password.isEmpty()) {
            User bean = new User();
            bean.setEmail(email);
            bean.setUserName(username);
            bean.setRole(role);
            bean.setPassword(password);
            bean.setStatus(status);
            userService.addUser(bean);
            resp.sendRedirect(req.getContextPath() + "/admin");
        } else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
