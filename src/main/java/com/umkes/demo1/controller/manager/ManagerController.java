package com.umkes.demo1.controller.manager;

import com.umkes.demo1.dao.AdvertisementDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.TypeDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.utils.Constants;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet("/mgr")
public class ManagerController extends HttpServlet {
    private AdvertisementServiceImpl advertisementService;

    @Inject
    public void init() {
        this.advertisementService = new AdvertisementServiceImpl(new AdvertisementDaoImpl(new UserDaoImpl(new RoleDaoImpl()), new TypeDaoImpl()));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int currentPage = req.getParameterMap().containsKey("currentPage") ? Integer.valueOf(req.getParameter("currentPage")) : 1;
        int recordsPerPage = req.getParameterMap().containsKey("recordsPerPage") ? Integer.valueOf(req.getParameter("recordsPerPage")) : 5;
        ArrayList<Advertisement> list = (ArrayList<Advertisement>) advertisementService.allAdvertisements(currentPage, recordsPerPage);
        int rowCount = advertisementService.allAdvertisementsCount();
        int noOfPages = rowCount / recordsPerPage;

        if (rowCount % recordsPerPage > 0) {
            noOfPages++;
        }
        req.setAttribute("noOfPages", noOfPages);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("recordsPerPage", recordsPerPage);
        req.setAttribute("advertisements", list);

        req.getRequestDispatcher(Constants.MANAGER_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
