package com.umkes.demo1.controller.manager;

import com.umkes.demo1.dao.AdvertisementDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.TypeDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.Advertisement;
import com.umkes.demo1.entity.Type;
import com.umkes.demo1.services.AdvertisementServiceImpl;
import com.umkes.demo1.services.TypeServiceImpl;
import com.umkes.demo1.utils.Constants;
import com.umkes.demo1.utils.EmailSender;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/mgr/edit")
public class EditAdvertisementController extends HttpServlet {
    private AdvertisementServiceImpl advertisementService;
    private TypeServiceImpl typeService;

    @Inject
    public void init() {
        this.advertisementService = new AdvertisementServiceImpl(new AdvertisementDaoImpl(new UserDaoImpl(new RoleDaoImpl()), new TypeDaoImpl()));
        this.typeService = new TypeServiceImpl(new TypeDaoImpl());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Advertisement adv = advertisementService.getAdvertisementById(Integer.parseInt(req.getParameter("edit")));

        req.setAttribute("adv", adv);
        req.setAttribute("types", typeService.allTypes());
        req.getRequestDispatcher(Constants.MANAGER_EDIT_ADVERTISEMENT_JSP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Advertisement adv = advertisementService.getAdvertisementById(Integer.parseInt(req.getParameter("edit")));
        Type type = typeService.getTypeById(Integer.parseInt(req.getParameter("selectedType")));
        String text = req.getParameter("advertisementText");
        String comment = req.getParameter("comment");
        int status = Integer.parseInt(req.getParameter("status"));

        adv.setText(text);
        adv.setType(type);
        adv.setStatus(status);
        adv.setComment(comment);
        advertisementService.editAdvertisement(adv);
        if (adv.getStatus() == -1) {
            sendAdvertisementEmail(adv);
        }
        resp.sendRedirect(req.getContextPath() + "/mgr");
    }

    private void sendAdvertisementEmail(Advertisement adv) {
        new Thread(() -> {
            EmailSender mail = new EmailSender();
            mail.setMailSubject("Advertisement details at Advertisement Board");
            mail.setMailText(
                    "Hello " + adv.getUser().getUserName() + "! \n" +
                            "Your advertisement was denied by this reason: " +
                            adv.getComment() + ".\n"
            );
            mail.setReceiverEmail(adv.getUser().getEmail());
            mail.send();
        }).start();
    }
}
