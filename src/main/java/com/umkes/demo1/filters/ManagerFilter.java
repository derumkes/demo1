package com.umkes.demo1.filters;

import com.umkes.demo1.dao.RegistrationTokenDaoImpl;
import com.umkes.demo1.dao.RoleDaoImpl;
import com.umkes.demo1.dao.UserDaoImpl;
import com.umkes.demo1.entity.User;
import com.umkes.demo1.services.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/ManagerFilter")
public class ManagerFilter implements Filter {
    private UserServiceImpl userService;

    @Override
    public void init(FilterConfig filterConfig){
        this.userService = new UserServiceImpl(new UserDaoImpl(new RoleDaoImpl()), new RegistrationTokenDaoImpl(new UserDaoImpl(new RoleDaoImpl())));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        User requestUser = (User) req.getSession().getAttribute("user");

        if (requestUser == null){
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }

        User user = userService.getUserByCredentials(requestUser.getEmail(), requestUser.getPassword());

        if (!user.isManager() || !user.isActive()) {
            resp.sendRedirect(req.getContextPath() + "/login");
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        this.userService = null;
    }
}
