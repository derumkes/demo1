package com.umkes.demo1;

import java.util.*;

public class TestStreamAPI {
    static {
        System.out.println("stat");
    }

    private List<Group> groups = new ArrayList<>();

    public TestStreamAPI() {
        populateData();
    }

    private void populateData() {
        for (int j = 0; j < 5; j++) {
            Group groupItem = new Group(UUID.randomUUID());
            List<Student> list = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                list.add(new Student(UUID.randomUUID(), Math.round(Math.random() * 5 * 100.0) / 100.0));
            }
            groupItem.setStudents(list);
            groups.add(groupItem);
        }
    }

    public static void main(String[] args) {
        TestStreamAPI tmp = new TestStreamAPI();
        Map<UUID, ArrayList<Student>> map = new HashMap<>();
        tmp.groups.stream()
                .forEach(e -> e.getStudents().stream()
                        .filter(s -> s.getRate() >= 4.5)
                        .forEach(s -> {
                                    if (map.containsKey(e.getId())) {
                                        map.get(e.getId()).add(s);
                                    } else {
                                        map.put(e.getId(), new ArrayList<Student>(Arrays.asList(s)));
                                    }
                                }
                        )
                );

        map.entrySet().stream()
                .forEach(g -> {
                    System.out.println("----- Group: " + g.getKey() + " -----");
                    g.getValue().stream().forEach(st -> System.out.println("Student: " + st.getId() + ", rate: " + st.getRate()));
                });
    }
}

class Group {
    private UUID id;
    private List<Student> students;

    public Group(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

}

class Student implements Comparable<Student> {
    private UUID id;
    private double rate;

    public Student(UUID id, double rate) {
        this.id = id;
        this.rate = rate;
    }

    public UUID getId() {
        return id;
    }

    public double getRate() {
        return rate;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", rate=" + rate +
                '}';
    }


    @Override
    public int compareTo(Student o) {
        return (int) ((rate - o.getRate()) * 100);
    }
}