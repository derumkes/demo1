package com.umkes.demo1.utils;

import com.umkes.demo1.exceptions.AdvertisementException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JDBCProperties {
    private static final String filename = "jdbc.properties";
    public static Properties getProps(){
        Properties prop = new Properties();
        InputStream str = null;
        try {
            str = JDBCProperties.class.getClassLoader().getResourceAsStream(filename);
            if(str == null){
                System.out.println("Sorry, unable to find " + filename);
                throw new FileNotFoundException(filename);
            }
            prop.load(str);
        } catch (IOException e) {
            throw new AdvertisementException("Couldn't get jdbc properties for db connection", e);
        } finally{
            if(str != null){
                try {
                    str.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }
}
