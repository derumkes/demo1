package com.umkes.demo1.utils;

import com.umkes.demo1.exceptions.AdvertisementException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {

    private static Connection connection;

    private ConnectionFactory() {
    }

    public static Connection getConnection() throws SQLException {
        if (connection != null) {
            return connection;
        } else {
            Properties prop = JDBCProperties.getProps();

            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                throw new AdvertisementException(e);
            }

            return DriverManager.getConnection(
                    prop.getProperty("db.url"),
                    prop.getProperty("db.user"),
                    prop.getProperty("db.password")
            );
        }
    }

    //For testing
    public static void setConnection(Connection conn){
        connection = conn;
    }
}


