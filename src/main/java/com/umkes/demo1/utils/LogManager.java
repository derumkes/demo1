package com.umkes.demo1.utils;

import java.util.logging.Logger;

public class LogManager {

    private static Logger LOGGER;

    static {
        String path = LogManager.class.getClassLoader()
                .getResource("logging.properties")
                .getFile();
        System.setProperty("java.util.logging.config.file", path);
    }

    public static Logger getLogger(String clsName) {
        return LOGGER.getLogger(clsName);
    }
}
