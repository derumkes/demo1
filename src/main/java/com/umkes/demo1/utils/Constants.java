package com.umkes.demo1.utils;

public final class Constants {
    public static final int ROLE_ADMIN = 1;
    public static final int ROLE_USER = 2;
    public static final int ROLE_MANAGER = 3;

    public static final String LOGIN_JSP = "/WEB-INF/pages/login/login.jsp";
    public static final String REGISTER_JSP = "/WEB-INF/pages/login/register.jsp";
    public static final String CONFIRM_REGISTRATION_JSP = "/WEB-INF/pages/login/confirm_registration.jsp";

    public static final String ADMIN_JSP = "/WEB-INF/pages/admin/admin.jsp";
    public static final String ADMIN_ADD_USER_JSP = "/WEB-INF/pages/admin/add_user.jsp";
    public static final String ADMIN_EDIT_USER_JSP = "/WEB-INF/pages/admin/edit_user.jsp";
    public static final String ERROR_PAGE = "/WEB-INF/pages/404.jsp";

    public static final String ADVERTISEMENT_JSP = "/WEB-INF/pages/advertisements/advertisement.jsp";
    public static final String ALL_ADVERTISEMENT_JSP = "/WEB-INF/pages/advertisements/all_advertisements.jsp";
    public static final String ADD_ADVERTISEMENT_JSP = "/WEB-INF/pages/advertisements/add_advertisement.jsp";
    public static final String EDIT_ADVERTISEMENT_JSP = "/WEB-INF/pages/advertisements/edit_advertisement.jsp";

    public static final String MANAGER_JSP = "/WEB-INF/pages/manager/manager.jsp";
    public static final String MANAGER_EDIT_ADVERTISEMENT_JSP = "/WEB-INF/pages/manager/edit_advertisement.jsp";

    public static final String HOST_URL = "http://localhost:8080/derumkes";


    private Constants(){}
}
