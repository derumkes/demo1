--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-03-03 17:27:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 11 (class 2615 OID 16576)
-- Name: advertiseBoard; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "advertiseBoard";


ALTER SCHEMA "advertiseBoard" OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 228 (class 1259 OID 16592)
-- Name: advertisements; Type: TABLE; Schema: advertiseBoard; Owner: postgres
--

CREATE TABLE "advertiseBoard".advertisements (
    advertisementid integer NOT NULL,
    text character(200) NOT NULL,
    userid integer NOT NULL,
    status integer DEFAULT 0,
    date date DEFAULT CURRENT_DATE,
    typeid integer NOT NULL,
    comment text
);


ALTER TABLE "advertiseBoard".advertisements OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16590)
-- Name: advertise_advertiseid_seq; Type: SEQUENCE; Schema: advertiseBoard; Owner: postgres
--

CREATE SEQUENCE "advertiseBoard".advertise_advertiseid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "advertiseBoard".advertise_advertiseid_seq OWNER TO postgres;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 227
-- Name: advertise_advertiseid_seq; Type: SEQUENCE OWNED BY; Schema: advertiseBoard; Owner: postgres
--

ALTER SEQUENCE "advertiseBoard".advertise_advertiseid_seq OWNED BY "advertiseBoard".advertisements.advertisementid;


--
-- TOC entry 233 (class 1259 OID 24581)
-- Name: registration_tokens; Type: TABLE; Schema: advertiseBoard; Owner: postgres
--

CREATE TABLE "advertiseBoard".registration_tokens (
    token uuid NOT NULL,
    userid integer,
    expire_date date DEFAULT (CURRENT_DATE + '2 days'::interval)
);


ALTER TABLE "advertiseBoard".registration_tokens OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16607)
-- Name: roles; Type: TABLE; Schema: advertiseBoard; Owner: postgres
--

CREATE TABLE "advertiseBoard".roles (
    roleid integer NOT NULL,
    rolename text NOT NULL
);


ALTER TABLE "advertiseBoard".roles OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16605)
-- Name: roles_roleid_seq; Type: SEQUENCE; Schema: advertiseBoard; Owner: postgres
--

CREATE SEQUENCE "advertiseBoard".roles_roleid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "advertiseBoard".roles_roleid_seq OWNER TO postgres;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 229
-- Name: roles_roleid_seq; Type: SEQUENCE OWNED BY; Schema: advertiseBoard; Owner: postgres
--

ALTER SEQUENCE "advertiseBoard".roles_roleid_seq OWNED BY "advertiseBoard".roles.roleid;


--
-- TOC entry 232 (class 1259 OID 16635)
-- Name: types; Type: TABLE; Schema: advertiseBoard; Owner: postgres
--

CREATE TABLE "advertiseBoard".types (
    typeid integer NOT NULL,
    typename character(50) NOT NULL
);


ALTER TABLE "advertiseBoard".types OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16633)
-- Name: types_typeid_seq; Type: SEQUENCE; Schema: advertiseBoard; Owner: postgres
--

CREATE SEQUENCE "advertiseBoard".types_typeid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "advertiseBoard".types_typeid_seq OWNER TO postgres;

--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 231
-- Name: types_typeid_seq; Type: SEQUENCE OWNED BY; Schema: advertiseBoard; Owner: postgres
--

ALTER SEQUENCE "advertiseBoard".types_typeid_seq OWNED BY "advertiseBoard".types.typeid;


--
-- TOC entry 226 (class 1259 OID 16579)
-- Name: users; Type: TABLE; Schema: advertiseBoard; Owner: postgres
--

CREATE TABLE "advertiseBoard".users (
    userid integer NOT NULL,
    email text NOT NULL,
    username text NOT NULL,
    roleid integer NOT NULL,
    password text NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE "advertiseBoard".users OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16577)
-- Name: users_userid_seq; Type: SEQUENCE; Schema: advertiseBoard; Owner: postgres
--

CREATE SEQUENCE "advertiseBoard".users_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "advertiseBoard".users_userid_seq OWNER TO postgres;

--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 225
-- Name: users_userid_seq; Type: SEQUENCE OWNED BY; Schema: advertiseBoard; Owner: postgres
--

ALTER SEQUENCE "advertiseBoard".users_userid_seq OWNED BY "advertiseBoard".users.userid;


--
-- TOC entry 2772 (class 2604 OID 16595)
-- Name: advertisements advertisementid; Type: DEFAULT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".advertisements ALTER COLUMN advertisementid SET DEFAULT nextval('"advertiseBoard".advertise_advertiseid_seq'::regclass);


--
-- TOC entry 2775 (class 2604 OID 16610)
-- Name: roles roleid; Type: DEFAULT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".roles ALTER COLUMN roleid SET DEFAULT nextval('"advertiseBoard".roles_roleid_seq'::regclass);


--
-- TOC entry 2776 (class 2604 OID 16638)
-- Name: types typeid; Type: DEFAULT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".types ALTER COLUMN typeid SET DEFAULT nextval('"advertiseBoard".types_typeid_seq'::regclass);


--
-- TOC entry 2770 (class 2604 OID 16582)
-- Name: users userid; Type: DEFAULT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".users ALTER COLUMN userid SET DEFAULT nextval('"advertiseBoard".users_userid_seq'::regclass);


--
-- TOC entry 2928 (class 0 OID 16592)
-- Dependencies: 228
-- Data for Name: advertisements; Type: TABLE DATA; Schema: advertiseBoard; Owner: postgres
--

INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (1, 'test1                                                                                                                                                                                                   ', 3, 1, '2020-02-14', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (3, '1                                                                                                                                                                                                       ', 3, 1, '2020-02-14', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (8, '1                                                                                                                                                                                                       ', 3, 0, '2020-02-20', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (11, '4                                                                                                                                                                                                       ', 3, 0, '2020-02-20', 2, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (12, '5                                                                                                                                                                                                       ', 3, 0, '2020-02-20', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (2, 'asdasdasdsd                                                                                                                                                                                             ', 3, 0, '2020-02-14', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (14, 'tra-ta-ta-ta-ta                                                                                                                                                                                         ', 3, 0, '2020-02-26', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (15, 'tra-ta-ta2                                                                                                                                                                                              ', 3, 0, '2020-02-26', 1, NULL);
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (5, 'dsfsdfsdf                                                                                                                                                                                               ', 3, -1, '2020-02-18', 1, 'bad comment
');
INSERT INTO "advertiseBoard".advertisements (advertisementid, text, userid, status, date, typeid, comment) VALUES (16, 'tetetetetete                                                                                                                                                                                            ', 17, -1, '2020-03-02', 1, 'comment                            
                        ');


--
-- TOC entry 2933 (class 0 OID 24581)
-- Dependencies: 233
-- Data for Name: registration_tokens; Type: TABLE DATA; Schema: advertiseBoard; Owner: postgres
--



--
-- TOC entry 2930 (class 0 OID 16607)
-- Dependencies: 230
-- Data for Name: roles; Type: TABLE DATA; Schema: advertiseBoard; Owner: postgres
--

INSERT INTO "advertiseBoard".roles (roleid, rolename) VALUES (1, 'admin');
INSERT INTO "advertiseBoard".roles (roleid, rolename) VALUES (2, 'user');
INSERT INTO "advertiseBoard".roles (roleid, rolename) VALUES (3, 'manager');


--
-- TOC entry 2932 (class 0 OID 16635)
-- Dependencies: 232
-- Data for Name: types; Type: TABLE DATA; Schema: advertiseBoard; Owner: postgres
--

INSERT INTO "advertiseBoard".types (typeid, typename) VALUES (1, 'Sales                                             ');
INSERT INTO "advertiseBoard".types (typeid, typename) VALUES (2, 'Rent                                              ');
INSERT INTO "advertiseBoard".types (typeid, typename) VALUES (3, 'Test                                              ');


--
-- TOC entry 2926 (class 0 OID 16579)
-- Dependencies: 226
-- Data for Name: users; Type: TABLE DATA; Schema: advertiseBoard; Owner: postgres
--

INSERT INTO "advertiseBoard".users (userid, email, username, roleid, password, status) VALUES (5, 'manager@test.com', 'manager', 3, '356a192b7913b04c54574d18c28d46e6395428ab', 1);
INSERT INTO "advertiseBoard".users (userid, email, username, roleid, password, status) VALUES (17, 'umkescv@gmail.com', 'sdfdsfds', 2, '356a192b7913b04c54574d18c28d46e6395428ab', 1);
INSERT INTO "advertiseBoard".users (userid, email, username, roleid, password, status) VALUES (18, 'mgr@test', 'mgrTest', 3, '356a192b7913b04c54574d18c28d46e6395428ab', 1);
INSERT INTO "advertiseBoard".users (userid, email, username, roleid, password, status) VALUES (3, 'test@test.com', 'Hello 1213', 2, '356a192b7913b04c54574d18c28d46e6395428ab', 1);
INSERT INTO "advertiseBoard".users (userid, email, username, roleid, password, status) VALUES (27, 'derumkes@gmail.com', 'admin', 1, '356a192b7913b04c54574d18c28d46e6395428ab', 1);


--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 227
-- Name: advertise_advertiseid_seq; Type: SEQUENCE SET; Schema: advertiseBoard; Owner: postgres
--

SELECT pg_catalog.setval('"advertiseBoard".advertise_advertiseid_seq', 16, true);


--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 229
-- Name: roles_roleid_seq; Type: SEQUENCE SET; Schema: advertiseBoard; Owner: postgres
--

SELECT pg_catalog.setval('"advertiseBoard".roles_roleid_seq', 3, true);


--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 231
-- Name: types_typeid_seq; Type: SEQUENCE SET; Schema: advertiseBoard; Owner: postgres
--

SELECT pg_catalog.setval('"advertiseBoard".types_typeid_seq', 4, true);


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 225
-- Name: users_userid_seq; Type: SEQUENCE SET; Schema: advertiseBoard; Owner: postgres
--

SELECT pg_catalog.setval('"advertiseBoard".users_userid_seq', 28, true);


--
-- TOC entry 2784 (class 2606 OID 16598)
-- Name: advertisements advertise_pk; Type: CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".advertisements
    ADD CONSTRAINT advertise_pk PRIMARY KEY (advertisementid);


--
-- TOC entry 2794 (class 2606 OID 24594)
-- Name: registration_tokens registration_tokens_pk; Type: CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".registration_tokens
    ADD CONSTRAINT registration_tokens_pk PRIMARY KEY (token);


--
-- TOC entry 2786 (class 2606 OID 16617)
-- Name: roles roles_pk; Type: CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".roles
    ADD CONSTRAINT roles_pk PRIMARY KEY (roleid);


--
-- TOC entry 2790 (class 2606 OID 16642)
-- Name: types types_pk; Type: CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".types
    ADD CONSTRAINT types_pk PRIMARY KEY (typeid);


--
-- TOC entry 2780 (class 2606 OID 16589)
-- Name: users users_pk; Type: CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".users
    ADD CONSTRAINT users_pk PRIMARY KEY (userid);


--
-- TOC entry 2782 (class 1259 OID 16596)
-- Name: advertise_advertiseid_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX advertise_advertiseid_uindex ON "advertiseBoard".advertisements USING btree (advertisementid);


--
-- TOC entry 2787 (class 1259 OID 16614)
-- Name: roles_roleid_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX roles_roleid_uindex ON "advertiseBoard".roles USING btree (roleid);


--
-- TOC entry 2788 (class 1259 OID 16615)
-- Name: roles_rolename_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX roles_rolename_uindex ON "advertiseBoard".roles USING btree (rolename);


--
-- TOC entry 2791 (class 1259 OID 16639)
-- Name: types_typeid_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX types_typeid_uindex ON "advertiseBoard".types USING btree (typeid);


--
-- TOC entry 2792 (class 1259 OID 16640)
-- Name: types_typename_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX types_typename_uindex ON "advertiseBoard".types USING btree (typename);


--
-- TOC entry 2778 (class 1259 OID 16586)
-- Name: users_email_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX users_email_uindex ON "advertiseBoard".users USING btree (email);


--
-- TOC entry 2781 (class 1259 OID 16587)
-- Name: users_userid_uindex; Type: INDEX; Schema: advertiseBoard; Owner: postgres
--

CREATE UNIQUE INDEX users_userid_uindex ON "advertiseBoard".users USING btree (userid);


--
-- TOC entry 2797 (class 2606 OID 16643)
-- Name: advertisements advertise_types_typeid_fk; Type: FK CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".advertisements
    ADD CONSTRAINT advertise_types_typeid_fk FOREIGN KEY (typeid) REFERENCES "advertiseBoard".types(typeid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2796 (class 2606 OID 16599)
-- Name: advertisements advertise_users_userid_fk; Type: FK CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".advertisements
    ADD CONSTRAINT advertise_users_userid_fk FOREIGN KEY (userid) REFERENCES "advertiseBoard".users(userid) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2798 (class 2606 OID 40963)
-- Name: registration_tokens registration_tokens_users_userid_fk; Type: FK CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".registration_tokens
    ADD CONSTRAINT registration_tokens_users_userid_fk FOREIGN KEY (userid) REFERENCES "advertiseBoard".users(userid) ON DELETE CASCADE;


--
-- TOC entry 2795 (class 2606 OID 16620)
-- Name: users users_roles_roleid_fk; Type: FK CONSTRAINT; Schema: advertiseBoard; Owner: postgres
--

ALTER TABLE ONLY "advertiseBoard".users
    ADD CONSTRAINT users_roles_roleid_fk FOREIGN KEY (roleid) REFERENCES "advertiseBoard".roles(roleid) ON UPDATE CASCADE;


--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 11
-- Name: SCHEMA "advertiseBoard"; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA "advertiseBoard" TO PUBLIC;


-- Completed on 2020-03-03 17:27:11

--
-- PostgreSQL database dump complete
--

